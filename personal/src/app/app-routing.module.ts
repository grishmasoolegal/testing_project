import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { UserlistComponent } from './userlist/userlist.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  
  },
  {
    path: 'profile',
    component: ProfileComponent,canActivate: [AuthGuard]
   
  
  },
  {
    path: 'Editprofile',
    component: EditprofileComponent,canActivate: [AuthGuard]
   
  
  },
  {
    path: 'Userlist',
    component: UserlistComponent,canActivate: [AuthGuard]
   
  
  },

  {
    path: 'register',
    component: RegisterComponent
   
  
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

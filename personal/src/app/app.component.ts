import { Component } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task';
  public cookie_name='';
 public all_cookies:any='';
 
constructor(private cookieService:CookieService){
 
}
 
setCookie(){
  this.cookieService.set('name','Testing');
}
 
deleteCookie(){
  this.cookieService.delete('name');
}
 
deleteAll(){
  this.cookieService.deleteAll();
}
 
ngOnInit(): void {
this.cookie_name=this.cookieService.get('name');
this.all_cookies=this.cookieService.getAll();  // get all cookies object
    }

}

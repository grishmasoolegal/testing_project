import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  token: any;

  constructor(private apiservice :AppService,private router : Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('USER')
  }
  form = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(localStorage.getItem('USERdata')),
    mobilenumber: new FormControl(''),
  
  });

  save(){
   
    console.log(this.form.value)
    this.apiservice.postMethod('profile/updateprofiledata?token='+this.token, this.form.value)
    .subscribe((data: any) => {
console.log(data,"after api")
this.router.navigate(['/profile']);

      
    })

  }

}

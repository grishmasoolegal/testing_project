import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public baseAPIPath = environment.apiBaseURL;

  constructor(private http: HttpClient) { }

  postMethod(apiname:any,data:any){
    const updateUrl = this.baseAPIPath + apiname;
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return this.http.post(updateUrl, data);
  }

  getMethod(apiname:any) {
    return this.http.get(this.baseAPIPath + apiname);
  }


  isLoggedIn() {
    var token = localStorage.getItem('USER');
    // console.log(token,"for chack auth guard")
    if (token) return true;
    else return false;
}
}

import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
  profilelist: any;

  constructor(private apiservice :AppService) { }

  ngOnInit(): void {
    this.getuserlist()
  }
  getuserlist(){
    this.apiservice.getMethod('userlogin/userlist')
    .subscribe((data: any) => {
      console.log(data ,"profile")
      this.profilelist = data

    })
  }

}

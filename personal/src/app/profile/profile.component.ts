import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
token:any
  userdata: any
  profiledata: any;
  file: any;
  imageSrc: any;
  constructor(private apiservice :AppService,private router : Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('USER')
   
    this.userdata = localStorage.getItem('USERdata')
    this.getdata()
  }
  editform = new FormGroup({
    profileimage: new FormControl({}),
    buffer:new FormControl({})
   
  
  });
  getdata(){
    let obj ={
      email:localStorage.getItem('USERdata'),
      token:this.token

    }
console.log(obj,"obk")
    this.apiservice.postMethod('profile/profiledata', obj)
    .subscribe((data: any) => {
      console.log(data ,"profile")
      this.profiledata = data

    })

  }

  deletedata(){
    // deleteprofiledata
    let obj={
      token:this.token
    }
    this.apiservice.postMethod('profile/deleteprofiledata', obj)
    .subscribe((data: any) => {
      console.log(data ,"profile")
      // this.profiledata = data
      this.router.navigate(['/']);

    })
  }
  profileChangeHandler(fileObj:any){
    if (fileObj.files[0] && fileObj.files[0].name) {
      if (fileObj.files.length > 0) {
        console.log(fileObj.files)
        

        let  reader: FileReader
    
     
          reader = new FileReader();
        reader.readAsArrayBuffer(fileObj.files[0]);
        reader.onload = () => {
          var arrayBuffer = reader.result;
          let doc={

            name: fileObj.files[0].name,
            type: fileObj.files[0].type,
            size: fileObj.files[0].size,
            fileObj: fileObj.files[0],
            data: arrayBuffer
           
          }

          this.editform.get('profileimage')?.setValue(doc)
      
       
          this.apiservice.postMethod('profile/updateprofileimage?token='+this.token, this.editform.value)
          .subscribe((data: any) => {
            console.log(data ,"profile")
         
      
          })

        }
       
      }
    
    }

  }


}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private apiservice :AppService,private router : Router) { }

  ngOnInit(): void {
  }

  form = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });

  save(){

    console.log(this.form.value)
    this.apiservice.postMethod('userlogin/registration', this.form.value)
    .subscribe((data: any) => {
console.log(data,"after api")
this.router.navigate(['/']);

      
    })

  }

}

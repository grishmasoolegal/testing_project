import { Component, ElementRef, OnInit , NgZone} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
// import { Application } from 'pixi.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
 


  
  constructor( private fb: FormBuilder,private apiservice :AppService,private router : Router, private elementRef: ElementRef, private ngZone: NgZone) {
    
   }

  ngOnInit(): void {

    
    
  }

   form = new FormGroup({
     email: new FormControl(''),
     password: new FormControl(''),
   });
  save(){
    console.log(this.form.value)

    this.apiservice.postMethod('userlogin/login', this.form.value)
        .subscribe((data: any) => {
          localStorage.setItem('USER',data.token)
          localStorage.setItem('USERdata',data.data.email)
console.log(data.data,"after api")
this.router.navigate(['/profile']);
          
        })
  }

}

const mongoose = require('mongoose');


const LoginSchema = mongoose.Schema({
    email: { type: mongoose.Schema.Types.String },
   name:{ type: mongoose.Schema.Types.String},
   password: { type: mongoose.Schema.Types.String},
   firstname: { type: mongoose.Schema.Types.String},
   lastname: { type: mongoose.Schema.Types.String},
   mobilenumber: { type: mongoose.Schema.Types.Number},
   accessToken: { type: mongoose.Schema.Types.String},
   created_at: { type: mongoose.Schema.Types.Date, default: Date.now },
   updated_at: { type: mongoose.Schema.Types.Date, default: Date.now },
});

module.exports = mongoose.model("Login", LoginSchema, "login");
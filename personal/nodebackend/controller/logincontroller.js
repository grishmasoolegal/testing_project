var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var loginService = require('../service/login.service');
var Constant = require('../constant/constant');




router.post('/login', async function (req, res) {

    loginService.login(req)
    
    .then(function(data) {
        if (data) {
            res.status(200).send(data);
        }else{
            
            res.status(204).send(Constant.noRecordMessage);
        }

    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
  
 });


 router.post('/registration', async function (req, res) {

    loginService.registration(req)
    .then(function(data) {
      
        if (data) {
            res.status(200).send(data);
        }else{
            
            res.status(204).send(Constant.noRecordMessage);
        }

    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
   
 });

 router.get('/userlist', async function (req, res) {

    loginService.userlist(req)
    .then(function(data) {
      
        if (data) {
            res.status(200).send(data);
        }else{
            
            res.status(204).send(Constant.noRecordMessage);
        }

    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
   
 });

//  userlist


 module.exports = router;

var express = require('express');
var router = express.Router();
const auth = require("../middileware/authentication");
var ProfileService = require('../service/profileservice');
var Constant = require('../constant/constant');


router.post('/profiledata',auth ,async function (req, res) {

    var response = {};
    ProfileService.getprofiledata(req)
    .then(function(data) {
        if (data) {
         
            res.status(200).send(data);
        } else {
          
            res.status(204).send(Constant.noRecordMessage);
        }
        
    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
   

   
  
     
})

router.post('/updateprofiledata',auth ,async function (req, res){
    // updateprofiledata

    var response = {};
    ProfileService.updateprofiledata(req)
    .then(function(data) {
        if (data) {
         
            res.status(200).send(data);
        } else {
          
            res.status(204).send(Constant.noRecordMessage);
        }
        
    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
})

router.post('/deleteprofiledata',auth ,async function (req, res){
    // updateprofiledata

    var response = {};
    ProfileService.deleteprofiledata(req)
    .then(function(data) {
        if (data) {
         
            res.status(200).send(data);
        } else {
          
            res.status(204).send(Constant.noRecordMessage);
        }
        
    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
})

router.post('/updateprofileimage',auth ,async function (req, res){
    // updateprofiledata

    var response = {};
    ProfileService.updateprofileimage(req)
    .then(function(data) {
        if (data) {
         
            res.status(200).send(data);
        } else {
          
            res.status(204).send(Constant.noRecordMessage);
        }
        
    })
    .catch(err =>
        res.status(500).send({
            respCode: Constant.respCode500,
            respMessage: err.message
        }));
})

// updateprofileimage



module.exports = router;
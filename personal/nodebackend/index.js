const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConfig = require("./config/db");

const app = express();

var corsOptions = {
    origin: "*"
  };
  
  app.use(cors(corsOptions));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  var logincontroller = require("./controller/logincontroller");
var profilecontroller = require("./controller/profilecontroller");
app.use("/userlogin", logincontroller);
app.use("/profile", profilecontroller);

  app.get("/", (req, res) => {
    res.json({ message: "Welcome to My Testing " });
  });
  

  const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
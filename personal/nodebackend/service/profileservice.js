const bcrypt = require('bcryptjs');
let jwt = require('jsonwebtoken');
loginModel = require('../model/login_schema')


async function getprofiledata(req) {
    // var response = [];
    try {
        console.log(req.body)

        let result = await loginModel.findOne({ email: req.body.email })
        if(result){
            return result
        }

    }catch (error) {
        throw error;
    }
  


}

async function updateprofiledata(req) {
    // var response = [];
    try {
        console.log(req.body)

        let result = await loginModel.updateOne({ accessToken: req.query.token },
       { 
firstname:req.body.firstname,
lastname:req.body.lastname,
mobilenumber:req.body.mobilenumber,

         }
            
            )
        if(result){
            return result
        }

    }catch (error) {
        throw error;
    }
  


}
async function deleteprofiledata(req) {
    // var response = [];
    try {
        console.log(req.body)

        let result = await loginModel.deleteOne({ accessToken: req.body.token } )
        if(result){
            return result
        }

    }catch (error) {
        throw error;
    }
  


}

async function updateprofileimage(req) {
    // var response = [];
    try {
      
      
     
        if (req.body.profileimage) {
            documentExt = req.body.profileimage.name.split(".");
                  fileName = req.body.profileimage.name.substring(i, req.body.profileimage.name.lastIndexOf("."));
                  documentExt = documentExt[documentExt.length - 1]
              filePath = "./upload/profile/" + fileName + "_" + new Date().getTime() + "_file." + documentExt;
            if (!fs.existsSync("./upload/profile/")) {
                        fs.mkdirSync("./upload/profile/", {
                          recursive: true
                        });
                      }
    
                fs.writeFileSync(filePath, req.body.profileimage.data);
                      delete req.body.profileimage['data'];
                      req.body.profileimage.filePath = filePath;
            }
       

        let result = await loginModel.updateOne({ accessToken: req.body.token } )
        if(result){
            return result
        }

    }catch (error) {
        throw error;
    }
  


}





module.exports = {
    getprofiledata,
    updateprofiledata,
    deleteprofiledata,
    updateprofileimage
    
}